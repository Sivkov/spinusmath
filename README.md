

## The Mathematica package for work with dissipative quantum spin dynamics.

Dissipation is implemented according the Ref R. Wieser, Eur. Phys. J. B 88, 77 (2015).
